import 'package:flutter/material.dart';
import 'package:app_quiniela_king/src/user/ui/screens/login_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Quiniela King',
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}