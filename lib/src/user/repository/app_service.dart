import 'dart:async';
import 'dart:convert';

import 'package:app_quiniela_king/src/user/repository/api_response.dart';
import 'package:app_quiniela_king/src/user/model/insert_user.dart';
import 'package:http/http.dart' as http;

class AppService {
  static const API = 'http://192.168.15.2:3000';
  static const headers = {
    'Content-Type': 'application/json'
  };

  Future<APIResponse<bool>> createUser(InsertUser item) async{
    return await http.post(API + '/mobile/sign_up', headers: headers, body: json.encode(item.toJson())).then((data) {
      if(data.statusCode == 200) {
        return APIResponse<bool>(data: true);
      }
      return APIResponse<bool>(error: true, errorMessage: 'Error API');
    })
    .catchError((_) => APIResponse<bool>(error: true, errorMessage: 'Error Server'));
  }
}