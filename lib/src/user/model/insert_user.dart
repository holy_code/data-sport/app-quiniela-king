import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class InsertUser {
  final String name;
  final String lastName;
  final String mobile;
  final String email;
  final String password;
  final String confirmPassword;
  final int identifier;

  InsertUser({
    @required this.name,
    @required this.lastName,
    @required this.mobile,
    @required this.email,
    @required this.password,
    @required this.confirmPassword,
    this.identifier }) :
      assert(name != null),
      assert(lastName != null),
      assert(mobile != null),
      assert(email != null),
      assert(password != null),
      assert(confirmPassword != null);

  Map<String, dynamic> toJson() {
    return {
	    "name": name,
      "lastName": lastName,
      "mobile": mobile,
      "email": email,
      "password": password,
      "confirmPassword": confirmPassword,
      "identifier": identifier
    };
  }
}